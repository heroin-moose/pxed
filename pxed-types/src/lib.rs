use ormlite::Model;
use ormlite::Error as OrmError;

use poem::{
    error::ResponseError,
    http::StatusCode
};

use sqlx::{
    FromRow,
    Type,
    Error as SqlxError,
    error::DatabaseError,
    sqlite::SqliteError
};

use serde::Deserialize;
use serde::Serialize;
use strum::Display;
use strum::EnumString;
use thiserror::Error;

pub enum ErrorCode {
    InternalServerError
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Error, Serialize, Deserialize)]
pub enum Error {
    #[error("Already exists")]
    AlreadyExists,

    #[error("Database error")]
    DatabaseError,

    #[error("Does not exist")]
    DoesNotExist,

    #[error("Device already exists")]
    DeviceAlreadyExists,

    #[error("Distribution already exists")]
    DistributionAlreadyExists,

    #[error("Installer already exists")]
    InstallerAlreadyExists,

    #[error("No such device")]
    NoSuchDevice,

    #[error("No such distribution")]
    NoSuchDistribution,

    #[error("No such installer")]
    NoSuchInstaller,

    #[error("No such profile")]
    NoSuchProfile,

    #[error("Profile already exists")]
    ProfileAlreadyExists,

    #[error("Template does not exist")]
    NoSuchTemplate
}

impl From<sqlx::Error> for Error {
    fn from(_: sqlx::Error) -> Self {
        Error::DatabaseError
    }
}

impl From<OrmError> for Error {
    fn from(e: OrmError) -> Self {
        if let OrmError::SqlxError(x) = e {
            if let SqlxError::Database(e) = x {
                let s = e.try_downcast::<SqliteError>();
                if let Ok(e) = s {
                    if let Some(c) = e.code() {
                        if c == "1555" {
                            return Error::AlreadyExists;
                        }
                    }
                }
            } else {
                return match x {
                    SqlxError::RowNotFound => Error::DoesNotExist,
                    _ => Error::DatabaseError
                }
            }
        }
        return Error::DatabaseError;
    }
}

impl ResponseError for Error {
    fn status(&self) -> StatusCode {
        match *self {
            Self::AlreadyExists => StatusCode::BAD_REQUEST,
            Self::DatabaseError => StatusCode::INTERNAL_SERVER_ERROR,
            Self::DeviceAlreadyExists => StatusCode::BAD_REQUEST,
            Self::DistributionAlreadyExists => StatusCode::BAD_REQUEST,
            Self::DoesNotExist => StatusCode::NOT_FOUND,
            Self::InstallerAlreadyExists => StatusCode::BAD_REQUEST,
            Self::NoSuchDevice => StatusCode::BAD_REQUEST,
            Self::NoSuchDistribution => StatusCode::BAD_REQUEST,
            Self::NoSuchInstaller => StatusCode::BAD_REQUEST,
            Self::NoSuchProfile => StatusCode::BAD_REQUEST,
            Self::NoSuchTemplate => StatusCode::INTERNAL_SERVER_ERROR,
            Self::ProfileAlreadyExists => StatusCode::BAD_REQUEST
        }
    }
}

#[derive(Debug, Clone, Copy, Display, EnumString, Serialize, Deserialize, Type)]
#[serde(rename_all = "lowercase")]
#[sqlx(rename_all = "lowercase")]
#[strum(serialize_all = "lowercase")]
pub enum Arch {
    AARCH64,
    E2K,
    I386,
    PPC64,
    PPC64LE,
    RISCV64,
    X86_64
}

#[derive(Debug, Display, EnumString, Serialize, Deserialize, Type)]
#[sqlx(rename_all = "lowercase")]
#[serde(rename_all = "lowercase")]
#[strum(serialize_all = "lowercase")]
pub enum InterfaceType {
    Os,
    Bmc,
    Bond,
    Bridge,
    Vlan
}

#[derive(Clone, Debug, Display, EnumString, Serialize, Deserialize, Type)]
#[sqlx(rename_all = "lowercase")]
#[serde(rename_all = "lowercase")]
#[strum(serialize_all = "lowercase")]
pub enum Bootloader {
    Grub,
    Petitboot
}

#[derive(Debug, Clone, Serialize, Deserialize, Type)]
#[sqlx(rename_all = "lowercase")]
#[serde(rename_all = "lowercase")]
pub enum ProvisioningStatus {
    Pending,
    Reboot,
    Installation,
    Firstboot,
    Configuration,
    Done
}

#[derive(Clone, Debug, Serialize, Deserialize, FromRow, Model)]
pub struct Installer {
    #[ormlite(primary_key)]
    pub name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, FromRow, Model)]
#[ormlite(table = "distributions")]
pub struct Distribution {
    #[ormlite(primary_key)]
    pub name: String,
    pub arch: Arch,
    pub os_name: String,
    pub os_family: Option<String>,
    pub os_release: String,
    pub installer: String,
    pub kernel: String,
    pub kernel_parameters: String,
    pub initramfs: Option<String>
}

#[derive(Clone, Debug, Serialize, Deserialize, FromRow, Model)]
#[ormlite(table = "profiles")]
pub struct Profile {
    #[ormlite(primary_key)]
    pub name: String,
    pub parent: Option<String>
}

#[derive(Debug, Clone, Serialize, Deserialize, FromRow, Model)]
#[ormlite(table = "devices")]
pub struct Device {
    #[ormlite(primary_key)]
    pub name: String,
    pub arch: Arch,
    pub fqdn: Option<String>,
    pub status: ProvisioningStatus,
    pub bootloader: Bootloader,
    pub distribution_name: String,
    pub profile_name: String,
    pub bmc_api: Option<String>,
    pub bmc_username: Option<String>,
    pub bmc_password: Option<String>
}
