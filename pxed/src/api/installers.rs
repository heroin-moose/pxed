use super::prelude::*;

pub fn route() -> Route {
    Route::new()
        .at("/", post(create).get(get_all))
        .at("/:name", get(get_one).delete(delete_one))
}

#[handler]
async fn create(installer: Json<Installer>, pool: Data<&SqlitePool>) -> Result<()> {
    (*installer).clone().insert(*pool).await.void().into_res()
}

#[handler]
async fn get_all(pool: Data<&SqlitePool>) -> Result<Json<Vec<Installer>>> {
    Installer::select().fetch_all(*pool).await.map(Json).into_res()
}

#[handler]
async fn get_one(Path(name): Path<String>, pool: Data<&SqlitePool>) -> Result<Json<Installer>> {
    Installer::get_one(&name, *pool).await.map(Json).into_res()
}

#[handler]
async fn delete_one(Path(name): Path<String>, pool: Data<&SqlitePool>) -> Result<()> {
    Installer::get_one(&name, *pool).await?.delete(*pool).await.into_res()
}
