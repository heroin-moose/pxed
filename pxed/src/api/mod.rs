pub mod answer_files;
pub mod devices;
pub mod distributions;
pub mod installers;
pub mod profiles;

mod prelude {
    pub use anyhow::Result;
    pub use anyhow::Context;

    pub use ormlite::model::*;

    pub use poem::Route;
    pub use poem::post;
    pub use poem::get;
    pub use poem::delete;
    pub use poem::handler;
    pub use poem::web::Data;
    pub use poem::web::Json;
    pub use poem::web::Path;

    pub use sqlx::SqlitePool;

    pub use pxed_types::*;

    pub use crate::ext::*;
}
