use super::prelude::*;

use tera::Tera;

pub fn route() -> Route {
    Route::new()
        .at("/by-device/:name/:installer/:file", get(render_by_device))
}

#[handler]
async fn render_by_device(
    Path((name, _, file)): Path<(String, String, String)>,
    pool: Data<&SqlitePool>,
    af_templates: Data<&Tera>
) -> Result<String> {
    let mut tx = pool.begin().await?;

    let device = Device::get_one(&name, &mut tx).await?;
    let profile = Profile::get_one(&device.profile_name, &mut tx).await?;
    let distro = Distribution::get_one(&device.distribution_name, &mut tx).await?;

    tx.rollback().await.ok();

    crate::template::render_answer_file(
        &af_templates,
        file,
        &distro,
        &profile,
        Some(&device)
    )
}
