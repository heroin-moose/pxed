use std::collections::HashMap;

use anyhow::Result;
use serde::Serialize;
use tera::Context;
use tera::Tera;
use tera::ErrorKind;
use walkdir::WalkDir;

use pxed_types::*;

use crate::ext::*;

#[derive(Serialize)]
struct OsContext {
    name: String,
    family: Option<String>,
    release: String
}

#[derive(Serialize)]
struct DistributionContext {
    name: String,
    arch: Arch
}

#[derive(Serialize)]
struct ProfileContext {
    name: String
}

#[derive(Serialize)]
struct DeviceContext {
    name: String,
    arch: Arch,
    fqdn: Option<String>
}

#[derive(Serialize)]
struct AnswerFileContext {
    distribution: DistributionContext,
    os: OsContext,
    profile: ProfileContext,
    device: Option<DeviceContext>
}

pub fn compile(defaults: impl AsRef<str>, custom: impl AsRef<str>) -> Result<Tera> {
    let mut filenames = HashMap::new();
    let mut tera = Tera::default();

    find_files(&mut filenames, custom.as_ref())?;
    find_files(&mut filenames, defaults.as_ref())?;

    tera.add_template_files(filenames.iter().map(|x| (x.1, Some(x.0))))?;

    Ok(tera)
}

// TODO: Deal with to_str() of abs_name and rel_name
// TODO: Skip hidden
fn find_files(map: &mut HashMap<String, String>, path: &str) -> Result<()> {
    let entries = WalkDir::new(path)
        .follow_links(true);

    for entry in entries {
        let entry = entry?;
        if !entry.file_type().is_file() {
            continue;
        }
        let filename = entry.path().to_str().unwrap();
        let basename = entry.path().strip_prefix(path)?.to_str().unwrap();
        if map.get(basename).is_none() {
            map.insert(basename.to_owned(), filename.to_owned());
        }
    }

    Ok(())
}

pub fn render_answer_file(
    tera: &Tera,
    file: impl AsRef<str>,
    distro: &Distribution,
    profile: &Profile,
    device: Option<&Device>
) -> Result<String> {
    let file = file.as_ref();

    let prefixes = if let Some(family) = &distro.os_family {
        vec![
            format!("{}/{}", family, distro.os_name),
            format!("{}", distro.os_name),
            format!("{}", family),
        ]
    } else {
        vec![
            format!("{}", distro.os_name)
        ]
    };

    let mut paths = Vec::new();
    for prefix in prefixes {
        let slug = prefix.to_lowercase();
        let mut release = distro.os_release.clone();
        loop {
            paths.push(format!("{}-{}-{}-{}", slug, release, distro.arch, file));
            paths.push(format!("{}-{}-{}", slug, release, file));
            if let Some(n) = release.rfind('.') {
                release.truncate(n);
                continue;
            }
            break;
        }
        paths.push(format!("{}-{}-{}", slug, distro.arch, file));
        paths.push(format!("{}-{}", slug, file));
    }

    let context = Context::from_serialize(
        AnswerFileContext {
            distribution: DistributionContext {
                name: distro.name.to_owned(),
                arch: distro.arch
            },
            os: OsContext {
                name: distro.os_name.to_owned(),
                family: distro.os_family.to_owned(),
                release: distro.os_release.to_owned()
            },
            profile: ProfileContext {
                name: profile.name.to_owned()
            },
            device: device.map(|device|
                DeviceContext {
                    name: device.name.to_owned(),
                    arch: device.arch,
                    fqdn: device.fqdn.to_owned()
                }
            )
        }
    )?;

    for path in &paths {
        let r = tera.render(path, &context);
        if let Err(e) = &r {
            if let ErrorKind::TemplateNotFound(_) = e.kind {
                continue;
            }
        }
        return r.into_res();
    }

    tera.render(file, &context).into_res()
}
