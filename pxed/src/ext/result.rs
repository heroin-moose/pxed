use std::convert::From;
use std::result::Result;

pub trait ResultExt<T, F> {
    fn into_res<U: From<F>>(self) -> Result<T, U>;
    fn void(self) -> Result<(), F>;
}

impl<T, F> ResultExt<T, F> for Result<T, F> {
    fn into_res<U: From<F>>(self) -> Result<T, U> {
        self.map_err(|e| e.into())
    }

    fn void(self) -> Result<(), F> {
        self.map(|_| ())
    }
}
