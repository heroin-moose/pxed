mod api;
mod ext;
mod template;

use std::fs::remove_file;

use anyhow::Result;
use poem::EndpointExt;
use poem::Route;
use poem::Server;
use poem::listener::UnixListener;
use poem::middleware::AddData;
use sqlx::SqlitePool;

#[tokio::main]
async fn main() -> Result<()> {
    remove_file("/tmp/pxed.sock").ok();

    let af_templates = template::compile("templates/answer-files", "templates/answer-files")?;
    let pool = SqlitePool::connect("sqlite:/tmp/pxed.sqlite").await?;
    let socket = UnixListener::bind("/tmp/pxed.sock");

    Server::new(socket).run(
        Route::new()
            .nest("/api/answer-files/", api::answer_files::route())
            .nest("/api/devices", api::devices::route())
            .nest("/api/distributions", api::distributions::route())
            .nest("/api/installers", api::installers::route())
            .nest("/api/profiles", api::profiles::route())
            .with(AddData::new(pool))
            .with(AddData::new(af_templates))
    ).await?;

    Ok(())
}
